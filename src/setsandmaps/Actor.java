/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setsandmaps;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class Actor implements Comparable<Actor>
{
    private String lastName;
    private String firstName;

    /**
     *
     * @param lastName the lastName of the actor
     * @param firstName the firstName of the actor
     */
    public Actor(String lastName, String firstName) 
    {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    /**
     *
     * @return the lastName of the actor
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName sets the lastName of the actor
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return the firstName of the actor
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName sets the firstName of the actor
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public int compareTo(Actor a){
        int lastN = this.lastName.compareTo(a.lastName);
        if(lastN==0){
            return this.firstName.compareTo(a.firstName);
        }
        else
        {
            return lastN;
        }
    
    }
    
    /*
    Returns a string representation of the course.
    @return a string representation of the course.
    */
    @Override
    public String toString() {
        return "Actor Name: " +lastName + "," + firstName;
    }
    
    
}
