/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setsandmaps;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class Movie 
{
    
    private String nameOfTheMovie;
    private String directorOfTheMovie;
    private double budgetOfTheMovie;
    private int lengthOfTheMovie;
    private String producerOfTheMovie[];

    /**
     *
     * @param nameOfTheMovie name of the movie
     * @param directorOfTheMovie director of the movie
     * @param budgetOfTheMovie budget of the movie
     * @param lengthOfTheMovie length of the movie
     * @param producerOfTheMovie producer of the movie
     */
    public Movie(String nameOfTheMovie, String directorOfTheMovie, 
            double budgetOfTheMovie, int lengthOfTheMovie, 
            String producerOfTheMovie[]) 
    {
        this.nameOfTheMovie = nameOfTheMovie;
        this.directorOfTheMovie = directorOfTheMovie;
        this.budgetOfTheMovie = budgetOfTheMovie;
        this.lengthOfTheMovie = lengthOfTheMovie;
        this.producerOfTheMovie = producerOfTheMovie;
    }

    /**
     *
     * @return the name of the movie
     */
    public String getNameOfTheMovie() {
        return nameOfTheMovie;
    }

    /**
     *
     * @param nameOfTheMovie sets the name of the movie
     */
    public void setNameOfTheMovie(String nameOfTheMovie) {
        this.nameOfTheMovie = nameOfTheMovie;
    }

    /**
     *
     * @return the director of the movie
     */
    public String getDirectorOfTheMovie() {
        return directorOfTheMovie;
    }

    /**
     *
     * @param directorOfTheMovie set the name of the director name
     */
    public void setDirectorOfTheMovie(String directorOfTheMovie) {
        this.directorOfTheMovie = directorOfTheMovie;
    }

    /**
     *
     * @return the budget of the movie
     */
    public double getBudgetOfTheMovie() {
        return budgetOfTheMovie;
    }

    /**
     *
     * @param budgetOfTheMovie sets the budget of the movie
     */
    public void setBudgetOfTheMovie(double budgetOfTheMovie) {
        this.budgetOfTheMovie = budgetOfTheMovie;
    }

    /**
     *
     * @return the length of the movie
     */
    public int getLengthOfTheMovie() {
        return lengthOfTheMovie;
    }

    /**
     *
     * @param lengthOfTheMovie sets the length of the movie
     */
    public void setLengthOfTheMovie(int lengthOfTheMovie) {
        this.lengthOfTheMovie = lengthOfTheMovie;
    }

    /**
     *
     * @return the name of producer of the movie
     */
    public String[] getProducerOfTheMovie() {
        return producerOfTheMovie;
    }

    /**
     *
     * @param producerOfTheMovie sets the name of producer of movie 
     */
    public void setProducerOfTheMovie(String[] producerOfTheMovie) {
        this.producerOfTheMovie = producerOfTheMovie;
    }
    /*
    Returns a string representation of the course.
    @return a string representation of the course.
    */

    @Override  
 public String toString() {
          String s="";
        for(String s1:producerOfTheMovie){
            s+=s1;
            s+=",";
        }
        s=s.substring(0, s.length()-1);
        String str=String.format ("Name: %s, Director: %s, " +"Budget: $%3.2f million, Length: %2d mins, Producer(s): %s", nameOfTheMovie,directorOfTheMovie,budgetOfTheMovie,lengthOfTheMovie,s);
        return str;
    }  
 

  
    
}
