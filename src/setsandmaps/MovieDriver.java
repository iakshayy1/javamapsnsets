/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setsandmaps;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;


/**
 *
 * @author  Akshay Reddy Vontari
 */
public class MovieDriver 
{

    public static void main(String[] args) throws FileNotFoundException
    {
        // Create a HashSet of Movie and name it as 'hashMovies'.
        HashSet<Movie> hashMovies = new HashSet<>();
        

        // Create a TreeMap of Actor as Key and HashSet<Movie> as value and name it as treeMaps.
        TreeMap<Actor, HashSet<Movie>> treeMaps = new TreeMap<>();
        /*
            Create a Scanner object to read Actors and Movie details from "movies.txt" file.
        */             
        Scanner sc = new Scanner(new File("movies.txt"));
        
     while(sc.hasNext())
        // while movies.txt has more data
        {
		/* The data in the text file will be in the following order: actor last name, actor first name, movie name, movie director name, movie title, movie budget, movie time run, and producer(s) of the movie separated by comma separator. */
            
            // Read in the data and store the 1st and 2nd line of the text 
            // file to Actor object.
            String LastName = sc.nextLine();
//            System.out.println(LastName);
            String FirstName = sc.nextLine();
//            System.out.println(FirstName);
            Actor a = new Actor(LastName,FirstName);
            // Read in the data and store the next 5 lines of the text file to 
            // a Movie object. Make sure that an array of Producers name is 
            // passed in to the movie object.
            String MovieName=sc.nextLine();
//            System.out.println(MovieName);
            String MovieDirector = sc.nextLine();
//            System.out.println(MovieDirector);
            double Budget = Double.parseDouble(sc.nextLine());
//            System.out.println(Budget);
            int MovieRunTime = Integer.parseInt(sc.nextLine());
//            System.out.println(MovieRunTime);
            String s = sc.nextLine();
//            System.out.println(s);
            String[] split = s.split(",");
            
            
          
            
            
            
            Movie m = new Movie(MovieName,MovieDirector,Budget,MovieRunTime,split);
            
            // Check the treeMap containsKey of actor for false. 
            if(treeMaps.containsKey(a)){
             
             
            
            // If it is false, then add the actor with new HashSet<Movie> to 
            // the treeMap.
           
            if(!treeMaps.get(a).isEmpty()){
                hashMovies=treeMaps.get(a);
                hashMovies.add(m);
                treeMaps.put(a, hashMovies);
            }
            else{
                HashSet<Movie> e;
                e = new HashSet<>();
                e.add(m);
                treeMaps.put(a, e);
            }
            }
            else
            {
                HashSet<Movie> q = new HashSet<>();
                q.add(m);
                treeMaps.put(a, q);
            }
               
            }
            

            
            // If the actor has no movies in the treeMap, then associate the 
            // movie to the actor in treeMap.
            // else if the actor has movie on his name, then the movie should 
            // be appended to the HashSet value of the actor in treeMap.
           
        
        // Print the Key and value pair using for loop
            System.out.println("Print the key and value pair using for loop:");
            for(Map.Entry<Actor,HashSet<Movie>> d:treeMaps.entrySet()){
                System.out.println(d.getKey()+"; Movie Details: "+d.getValue());
            }
            System.out.println("");
            System.out.println("*************************************************");
        // Print the TreeMap object
            System.out.println("Print the TreeMap:");
            System.out.println(treeMaps);
            System.out.println("");
            System.out.println("*************************************************");
        
    }
}
            






